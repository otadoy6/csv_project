<?php
require_once '../config/db.php';

$pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);

$sql = 'SELECT lastname,firstname,email FROM employees ORDER BY lastname';

$statement = $pdo->prepare($sql);

$statement->execute();

$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

$columnNames = ['Last Name', 'First Name', 'Email'];

$fileName = 'employee_data.csv';

header('Content-Type: application/excel');
header('Content-Disposition: attachment; filename="' . $fileName . '"');

$fp = fopen('php://output', 'w');

fputcsv($fp, $columnNames);

foreach ($rows as $row) {
    fputcsv($fp, $row);
}


fclose($fp);
?>